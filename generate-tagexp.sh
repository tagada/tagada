#!/bin/sh

cd src/automatically_generated
   # Generate the parser
   adagoop ../tag_expressions.formal_description Tagexp
   scaflex tagexp.l
   gnatchop -w tagexp.a
   gnatchop -w tagexp_io.a
   gnatchop -w tagexp_dfa.a
   scayacc tagexp.y
   gnatchop -w tagexp.a
   gnatchop -w tagexp_goto.a
   sed -e 1d tagexp_tokens.a > tagexp_modified_tokens.a
   gnatchop -w tagexp_modified_tokens.a
   gnatchop -w tagexp_shift_reduce.a
   # Patch the parser to use command-line arguments rather than a file
   patch < ../diffs/tagexp_io.adb.diff
   # Patch the parser to raise unhandled Syntax_Error exceptions where:
   #    - an exception should be raised but would not be otherwise
   #    - the exception handler would print irrelevant information
   #    - the exception handler would not propagate the exception
   # thereby exiting properly when a syntax error is spotted.
cd ../..
