#!/bin/sh

# Get rid of a possible previous database
rm -rf exec/tagada.db
# Create a database from the schema
gnatcoll_db2ada -dbtype=sqlite -dbname=exec/tagada.db -dbmodel=src/db.schema -createdb
