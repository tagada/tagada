TagAda allows you to organize your data using tag-based expressions.

Unlike other (noble) attempts at creating semantic file systems to achieve such a goal, TagAda does NOT rely on a specific file system which results in ease of deployment and orthogonality against the traditional directory hierarchy.

Although TagAda was primarily designed to find regular files, it turned out to be pretty handy with bookmarks and other identifiable resources.

Command-line usage:

tagada 1992 AND Film NOT Action

This would retrieve your non-action films released in 1992, 
provided you tagged your files accordingly...

As usual, NOT has precedence over AND which has itself precedence over OR. 
Any ambiguity which may arise due to the non-associativity of the NOT operator would be resolved by evaluating the expression from the left to the right.

Precedence can be overridden by surrounding the lower priority parts of an expression with parentheses.

Through Midnight Commander:

Consecutively press: CTRL+x ! Tab 
Enter a TagAda command, e.g. tagada Music AND (Latin OR Jazz) NOT wma NOT mp3 
Press Return.

Enjoy your power.

Nautilus scripts usage:

Play with your mouse and have a look at the directory TagAda_Results (automatically created) in your Desktop.

Development:

-add a NOT operator to specify which tags you don't want to retrieve from. 
(DONE)

-create an LALR(1) grammar to generate a full parser. This would allow patterns such as: 
(Tag1 OR Tag2) AND Tag3 AND (((Tag4))) NOT (Tag4 OR Tag5) 
(DONE)

-replace Leaftag (raw SQLite) with GNATCOLL.SQL.ORM (object-relational mapping) to make TagAda platform-independent, at either the OS side or the DBMS side. 
(DONE)

-avoid race conditions and deadlocks when using the SQLite backend of the ORM (by using a protected object). 
(PENDING)

-provide the user with a complete manual. Special-use information should be given. 
(PENDING)

-use kernel event notification facilities (inotify under Linux, kqueue under BSD) to handle moved files and deleted files efficiently. 
(TO DO)

-Embed a web server (using the Ada Web Server framework) into the TagAda executable to make TagAda usable through a web interface. 
(TO DO)

-create a GTK+ interface 
(TO DO)
