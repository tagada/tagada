#!/bin/sh

# Use checksums instead of timestamps (-m switch) to avoid
# generated code to be compiled unnecessarily
gnatmake -m -P TagAda.gpr

# Run the executable
#    ./exec/tagada_search
#    valgrind --leak-check=full --show-reachable=yes ./exec/tagada_search
