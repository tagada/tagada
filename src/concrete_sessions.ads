--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Abstract_Association_Sessions; use Abstract_Association_Sessions;
with Abstract_Resource_Sessions;    use Abstract_Resource_Sessions;
with Abstract_Tag_Sessions;         use Abstract_Tag_Sessions;
with GNATCOLL.SQL.Sessions;         use GNATCOLL.SQL.Sessions;

package Concrete_Sessions is

   type Concrete_Session is new Session_Type
                                 and Abstract_Association_Session
                                 and Abstract_Resource_Session
                                 and Abstract_Tag_Session with null record;

   type Concrete_Session_Access is access Concrete_Session;

   --------------------------------------------------------
   -- overriding Abstract_Association_Session primitives --
   --------------------------------------------------------

   overriding procedure Add_Association (Session        : Concrete_Session;
                                         Tag_Name       : String;
                                         Resource_Uri   : String);

   overriding procedure Commit (Session : Concrete_Session);

   overriding procedure Delete_Association (Session        : Concrete_Session;
                                            Tag_Name       : String;
                                            Resource_Uri   : String);

   overriding procedure Put_All_Associations (Session : Concrete_Session);

   overriding procedure Put_All_Raw_Associations (Session : Concrete_Session);

   -----------------------------------------------------
   -- overriding Abstract_Resource_Session primitives --
   -----------------------------------------------------

   overriding procedure Add_Resource (Session      : Concrete_Session;
                                      Resource_Uri : String);

   overriding procedure Add_Resource_Description
                          (Session              : Concrete_Session;
                           Resource_Uri         : String;
                           Resource_Description : String);

   overriding procedure Change_Resource_Uri
                          (Session          : Concrete_Session;
                           Resource_Uri     : String;
                           New_Resource_Uri : String);

   overriding procedure Delete_Resource (Session      : Concrete_Session;
                                         Resource_Uri : String);

   overriding procedure Put_All_Resources (Session : Concrete_Session);

   overriding procedure Put_Tags (Session      : Concrete_Session;
                                  Resource_Uri : String);

   overriding function Resource_Uri_Is_Available
                         (Session      : Concrete_Session;
                          Resource_Uri : String)
                         return Boolean;

   ------------------------------------------------
   -- overriding Abstract_Tag_Session primitives --
   ------------------------------------------------

   overriding procedure Add_Tag (Session  : Concrete_Session;
                                 Tag_Name : String);

   overriding procedure Add_Tag_Description
                          (Session         : Concrete_Session;
                           Tag_Name        : String;
                           Tag_Description : String);

   overriding procedure Change_Tag_Name (Session      : Concrete_Session;
                                         Tag_Name     : String;
                                         New_Tag_Name : String);

   overriding procedure Delete_Tag (Session  : Concrete_Session;
                                    Tag_Name : String);

   overriding procedure Put_All_Tags (Session           : Concrete_Session;
                                      With_Descriptions : Boolean := False);

   overriding procedure Put_Resources (Session  : Concrete_Session;
                                       Tag_Name : String);

   overriding function Tag_Name_Is_Available (Session  : Concrete_Session;
                                              Tag_Name : String)
                                             return Boolean;

private

   --------------------------------------------------------
   -- overriding Abstract_Association_Session primitives --
   --------------------------------------------------------

   overriding procedure Add_New_Association (Session     : Concrete_Session;
                                             Tag_Id      : Integer;
                                             Resource_Id : Integer);

   -----------------------------------------------------
   -- overriding Abstract_Resource_Session primitives --
   -----------------------------------------------------

   overriding procedure Add_New_Resource (Session      : Concrete_Session;
                                          Resource_Uri : String);

   overriding procedure Internal_Delete_Resource
                          (Session      : Concrete_Session;
                           Resource_Uri : String);

   overriding procedure Internal_Put_Tags (Session      : Concrete_Session;
                                           Resource_Uri : String);

   overriding function Internal_Resource_Id (Session      : Concrete_Session;
                                             Resource_Uri : String)
                                            return Integer;

   ------------------------------------------------
   -- overriding Abstract_Tag_Session primitives --
   ------------------------------------------------

   overriding procedure Add_New_Tag (Session  : Concrete_Session;
                                     Tag_Name : String);

   overriding procedure Internal_Delete_Tag (Session  : Concrete_Session;
                                             Tag_Name : String);

   overriding procedure Internal_Put_Resources (Session  : Concrete_Session;
                                                Tag_Name : String);

   overriding function Internal_Tag_Id (Session  : Concrete_Session;
                                        Tag_Name : String)
                                       return Integer;

end Concrete_Sessions;
