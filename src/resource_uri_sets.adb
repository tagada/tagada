--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Ada.Text_IO;
with GNATCOLL.SQL.Sessions; use GNATCOLL.SQL.Sessions;
with ORM;                   use ORM;

package body Resource_Uri_Sets is

   ----------------
   -- Difference --
   ----------------

   function Difference (Left, Right : Resource_Uri_Set) return Resource_Uri_Set
   is
   begin
      return Left - Right;
   end Difference;

   --------------------------
   -- Get_Resource_Uri_Set --
   --------------------------

   function Get_Resource_Uri_Set (Session  : Concrete_Session;
                                  Tag_Name : String)
                                 return Resource_Uri_Set
   is
      Set : Resource_Uri_Set;
      --  If not otherwise initialized,
      --  an object of type Resource_Uri_Set
      --  initially has the same value as Empty_Set,
      --  which is not visible here.
      TL : constant Tag_List := All_Tags.Filter
                                  (Name => Tag_Name)
                                .Get (Session_Type (Session));
   begin
      if TL.Has_Row then
         Set := Internal_Get_Resource_Uri_Set (Session, Tag_Name);
      end if;
      return Set;
   end Get_Resource_Uri_Set;

   -----------------------------------
   -- Internal_Get_Resource_Uri_Set --
   -----------------------------------

   function Internal_Get_Resource_Uri_Set
              (Session  : Concrete_Session;
               Tag_Name : String)
              return Resource_Uri_Set
   is
      Set : Resource_Uri_Set;
      --  If not otherwise initialized,
      --  an object of type Resource_Uri_Set
      --  initially has the same value as Empty_Set,
      --  which is not visible here.
      TL : constant Tag_List := All_Tags.Filter
                                  (Name => Tag_Name)
                                .Get (Session_Type (Session));
      AM : constant Associations_Managers := TL.Element.Associated_Resources;
      AL : Association_List := AM.Get (Session_Type (Session));
   begin
      while AL.Has_Row loop
         Set.Insert (AL.Element.Resource_Field);
         AL.Next;
      end loop;
      return Set;
   end Internal_Get_Resource_Uri_Set;

   ------------------
   -- Intersection --
   ------------------

   function Intersection (Left, Right : Resource_Uri_Set)
                         return Resource_Uri_Set
   is
   begin
      return Left and Right;
   end Intersection;

   -------------------
   -- Put_Resources --
   -------------------

   procedure Put_Resources (Set     : Resource_Uri_Set;
                            Session : Concrete_Session)
   is
      Cursor : Integer_Sets.Cursor := Set.First;
   begin
      while Integer_Sets.Has_Element (Cursor) loop
         Ada.Text_IO.Put_Line (Get_Resource (Session_Type (Session),
                                             Integer_Sets.Element (Cursor))
                               .Uri);
         Integer_Sets.Next (Cursor);
      end loop;
   end Put_Resources;

   -----------
   -- Union --
   -----------

   function Union (Left, Right : Resource_Uri_Set) return Resource_Uri_Set
   is
   begin
      return Left or Right;
   end Union;

end Resource_Uri_Sets;
