--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

package Abstract_Association_Sessions is

   type Abstract_Association_Session is interface;

   procedure Add_Association (Session : Abstract_Association_Session;
                              Tag_Name       : String;
                              Resource_Uri   : String) is abstract;

   procedure Add_New_Association (Session     : Abstract_Association_Session;
                                  Tag_Id      : Integer;
                                  Resource_Id : Integer) is abstract;

   procedure Commit (Session : Abstract_Association_Session) is abstract;

   procedure Delete_Association (Session : Abstract_Association_Session;
                                 Tag_Name       : String;
                                 Resource_Uri   : String) is abstract;

   procedure Put_All_Associations
               (Session : Abstract_Association_Session) is abstract;

   procedure Put_All_Raw_Associations
               (Session : Abstract_Association_Session) is abstract;

end Abstract_Association_Sessions;
