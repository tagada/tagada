--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Resource_Uri_Sets; use Resource_Uri_Sets;
private with Set_Lists;

package Set_Stacks is

   type Set_Stack is tagged private;

   procedure Push (S   : in out Set_Stack;
                   Set : Resource_Uri_Set);

   function Pop (S : in out Set_Stack)
                return Resource_Uri_Set;

   function Size (S : Set_Stack) return Natural;

   Set_Stack_Empty : exception;

private

   type Set_Stack is new Set_Lists.List with null record;

end Set_Stacks;
