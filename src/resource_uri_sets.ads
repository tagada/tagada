--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Concrete_Sessions; use Concrete_Sessions;
private with Integer_Sets;

package Resource_Uri_Sets is

   type Resource_Uri_Set is tagged private;

   function Difference (Left, Right : Resource_Uri_Set)
                       return Resource_Uri_Set;

   function Get_Resource_Uri_Set
              (Session  : Concrete_Session;
               Tag_Name : String)
              return Resource_Uri_Set;

   function Internal_Get_Resource_Uri_Set
              (Session  : Concrete_Session;
               Tag_Name : String)
              return Resource_Uri_Set;

   function Intersection (Left, Right : Resource_Uri_Set)
                         return Resource_Uri_Set;

   procedure Put_Resources (Set     : Resource_Uri_Set;
                            Session : Concrete_Session);

   function Union (Left, Right : Resource_Uri_Set)
                  return Resource_Uri_Set;

private

   type Resource_Uri_Set is new Integer_Sets.Set with null record;

end Resource_Uri_Sets;
