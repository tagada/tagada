--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Ada.Text_IO; use Ada.Text_IO;
with ORM;         use ORM;

package body Concrete_Sessions is

   --------------------------------------------------------
   -- overriding Abstract_Association_Session primitives --
   --------------------------------------------------------

   ---------------------
   -- Add_Association --
   ---------------------

   procedure Add_Association (Session        : Concrete_Session;
                              Tag_Name       : String;
                              Resource_Uri   : String)
   is
   begin
      if not All_Associations.Filter
               (Tag_Field      => Session
                                  .Internal_Tag_Id (Tag_Name),
                Resource_Field => Session
                                  .Internal_Resource_Id (Resource_Uri))
             .Get (Session_Type (Session))
             .Has_Row
      then
         Add_New_Association
           (Session,
            Session.Internal_Tag_Id (Tag_Name),
            Session.Internal_Resource_Id (Resource_Uri));
      end if;
   end Add_Association;

   -------------------------
   -- Add_New_Association --
   -------------------------

   procedure Add_New_Association (Session     : Concrete_Session;
                                  Tag_Id      : Integer;
                                  Resource_Id : Integer)
   is
      Association : constant Detached_Association'Class := New_Association;
   begin
      Association.Set_Tag_Field (Tag_Id);
      Association.Set_Resource_Field (Resource_Id);
      Session.Persist (Association);
   end Add_New_Association;

   ------------
   -- Commit --
   ------------

   procedure Commit (Session : Concrete_Session)
   is
   begin
      Session_Type (Session).Commit;
   end Commit;

   ------------------------
   -- Delete_Association --
   ------------------------

   procedure Delete_Association (Session        : Concrete_Session;
                                 Tag_Name       : String;
                                 Resource_Uri   : String)
   is
   begin
      if not Tag_Name_Is_Available
               (Session, Tag_Name)
         and then not Resource_Uri_Is_Available
                        (Session, Resource_Uri)
            and then All_Associations.Filter
                       (Tag_Field      => Session
                                          .Internal_Tag_Id
                                            (Tag_Name),
                        Resource_Field => Session
                                          .Internal_Resource_Id
                                            (Resource_Uri))
                     .Get (Session_Type (Session))
                     .Has_Row
      then
         All_Associations.Filter
           (Tag_Field      => Session
                              .Internal_Tag_Id (Tag_Name),
            Resource_Field => Session
                              .Internal_Resource_Id (Resource_Uri))
         .Get (Session_Type (Session))
         .Element
         .Detach
         .Internal_Delete;
      end if;
   end Delete_Association;

   --------------------------
   -- Put_All_Associations --
   --------------------------

   procedure Put_All_Associations (Session : Concrete_Session)
   is
      AL : Association_List := All_Associations.Get (Session_Type (Session));
   begin
      while AL.Has_Row loop
            Put_Line (Get_Tag (Session_Type (Session),
                               AL.Element.Tag_Field)
                      .Name &
                      " ---------------- " &
                      Get_Resource (Session_Type (Session),
                                    AL.Element.Resource_Field)
                      .Uri);
            AL.Next;
      end loop;
   end Put_All_Associations;

   ------------------------------
   -- Put_All_Raw_Associations --
   ------------------------------

   procedure Put_All_Raw_Associations (Session : Concrete_Session)
   is
      AL : Association_List := All_Associations.Get (Session_Type (Session));
   begin
      while AL.Has_Row loop
            Put_Line (Integer'Image (AL.Element.Tag_Field) &
                      " ---------------- " &
                      Integer'Image (AL.Element.Resource_Field));
            AL.Next;
      end loop;
   end Put_All_Raw_Associations;

   -----------------------------------------------------
   -- overriding Abstract_Resource_Session primitives --
   -----------------------------------------------------

   ------------------
   -- Add_Resource --
   ------------------

   procedure Add_Resource (Session      : Concrete_Session;
                           Resource_Uri : String)
   is
   begin
      if Resource_Uri_Is_Available
           (Session, Resource_Uri)
      then
         Session.Add_New_Resource (Resource_Uri);
      end if;
   end Add_Resource;

   ----------------------
   -- Add_New_Resource --
   ----------------------

   procedure Add_New_Resource (Session      : Concrete_Session;
                               Resource_Uri : String)
   is
      Resource : constant Detached_Resource'Class := New_Resource;
   begin
      Resource.Set_Uri (Resource_Uri);
      Session.Persist (Resource);
   end Add_New_Resource;

   ------------------------------
   -- Add_Resource_Description --
   ------------------------------

   procedure Add_Resource_Description (Session              : Concrete_Session;
                                       Resource_Uri         : String;
                                       Resource_Description : String)
   is
      RL : constant Resource_List := All_Resources.Filter (Uri => Resource_Uri)
                                     .Get (Session_Type (Session));

   begin
      if RL.Has_Row then
         RL.Element.Detach.Set_Description (Resource_Description);
      end if;
   end Add_Resource_Description;

   -------------------------
   -- Change_Resource_Uri --
   -------------------------

   procedure Change_Resource_Uri (Session          : Concrete_Session;
                                  Resource_Uri     : String;
                                  New_Resource_Uri : String)
   is
      RL : constant Resource_List := All_Resources.Filter (Uri => Resource_Uri)
                                     .Get (Session_Type (Session));

   begin
      if RL.Has_Row then
         if Resource_Uri_Is_Available
              (Session, New_Resource_Uri)
         then
            RL.Element.Detach.Set_Uri (New_Resource_Uri);
         else
            Session.Internal_Delete_Resource (New_Resource_Uri);
            RL.Element.Detach.Set_Uri (New_Resource_Uri);
         end if;
      end if;
   end Change_Resource_Uri;

   ---------------------
   -- Delete_Resource --
   ---------------------

   procedure Delete_Resource (Session      : Concrete_Session;
                              Resource_Uri : String)
   is
   begin
      if not Resource_Uri_Is_Available
               (Session, Resource_Uri)
      then
         Session.Internal_Delete_Resource (Resource_Uri);
      end if;
   end Delete_Resource;

   ------------------------------
   -- Internal_Delete_Resource --
   ------------------------------

   procedure Internal_Delete_Resource (Session      : Concrete_Session;
                                       Resource_Uri : String)
   is
      Resource_Id : constant Integer := Internal_Resource_Id
                                          (Session, Resource_Uri);
      AL : Association_List := All_Associations.Filter
                                 (Resource_Field => Resource_Id)
                               .Get (Session_Type (Session));
   begin
      while AL.Has_Row loop
         AL.Element.Detach.Internal_Delete;
         AL.Next;
      end loop;
      Get_Resource (Session_Type (Session), Resource_Id)
      .Internal_Delete;
   end Internal_Delete_Resource;

   -----------------------
   -- Internal_Put_Tags --
   -----------------------

   procedure Internal_Put_Tags (Session      : Concrete_Session;
                                Resource_Uri : String)
   is
      AL : Association_List := All_Resources.Filter (Uri => Resource_Uri)
                               .Get (Session_Type (Session))
                               .Element.Associated_Tags
                               .Get (Session_Type (Session));
   begin
      while AL.Has_Row loop
         Put_Line (Get_Tag (Session_Type (Session),
                            AL.Element.Tag_Field)
                   .Name);
         AL.Next;
      end loop;
   end Internal_Put_Tags;

   --------------------------
   -- Internal_Resource_Id --
   --------------------------

   function Internal_Resource_Id (Session      : Concrete_Session;
                                  Resource_Uri : String)
                                 return Integer
   is
      RL  : constant Resource_List := All_Resources.Filter
                                        (Uri => Resource_Uri)
                                      .Get (Session_Type (Session));
      Internal_Resource_Id_Error : exception;
   begin
      if RL.Has_Row then
         return RL.Element.Id;
      else
         raise Internal_Resource_Id_Error;
      end if;
   end Internal_Resource_Id;

   -----------------------
   -- Put_All_Resources --
   -----------------------

   procedure Put_All_Resources (Session : Concrete_Session)
   is
      RL : Resource_List := All_Resources.Get (Session_Type (Session));
   begin
      while RL.Has_Row loop
            Put_Line (RL.Element.Uri &
                      " (" &
                      Integer'Image (RL.Element.Id) &
                      " )");
            RL.Next;
      end loop;
   end Put_All_Resources;

   --------------
   -- Put_Tags --
   --------------

   procedure Put_Tags (Session      : Concrete_Session;
                       Resource_Uri : String)
   is
      RL : constant Resource_List := All_Resources.Filter (Uri => Resource_Uri)
                                     .Get (Session_Type (Session));
   begin
      if RL.Has_Row then
         Session.Internal_Put_Tags (Resource_Uri);
      end if;
   end Put_Tags;

   -------------------------------
   -- Resource_Uri_Is_Available --
   -------------------------------

   function Resource_Uri_Is_Available (Session      : Concrete_Session;
                                       Resource_Uri : String)
                                      return Boolean
   is
      RL : constant Resource_List := All_Resources.Filter (Uri => Resource_Uri)
                                     .Get (Session_Type (Session));
   begin
      return not RL.Has_Row;
   end Resource_Uri_Is_Available;

   ------------------------------------------------
   -- overriding Abstract_Tag_Session primitives --
   ------------------------------------------------

   -------------
   -- Add_Tag --
   -------------

   procedure Add_Tag (Session  : Concrete_Session;
                      Tag_Name : String)
   is
   begin
      if Tag_Name_Is_Available
           (Session, Tag_Name)
      then
         Session.Add_New_Tag (Tag_Name);
      end if;
   end Add_Tag;

   -----------------
   -- Add_New_Tag --
   -----------------

   procedure Add_New_Tag (Session  : Concrete_Session;
                          Tag_Name : String)
   is
      Tag : constant Detached_Tag'Class := New_Tag;
   begin
      Tag.Set_Name (Tag_Name);
      Session.Persist (Tag);
   end Add_New_Tag;

   -------------------------
   -- Add_Tag_Description --
   -------------------------

   procedure Add_Tag_Description (Session         : Concrete_Session;
                                  Tag_Name        : String;
                                  Tag_Description : String)
   is
      TL : constant Tag_List := All_Tags.Filter (Name => Tag_Name)
                                .Get (Session_Type (Session));
   begin
      if TL.Has_Row then
         TL.Element.Detach.Set_Description (Tag_Description);
      end if;
   end Add_Tag_Description;

   ---------------------
   -- Change_Tag_Name --
   ---------------------

   procedure Change_Tag_Name (Session      : Concrete_Session;
                              Tag_Name     : String;
                              New_Tag_Name : String)
   is
      TL : constant Tag_List := All_Tags.Filter (Name => Tag_Name)
                                .Get (Session_Type (Session));
   begin
      if TL.Has_Row then
         if Tag_Name_Is_Available
              (Session, New_Tag_Name)
         then
            TL.Element.Detach.Set_Name (New_Tag_Name);
         else
            Session.Internal_Delete_Tag (New_Tag_Name);
            TL.Element.Detach.Set_Name (New_Tag_Name);
         end if;
      end if;
   end Change_Tag_Name;

   ----------------
   -- Delete_Tag --
   ----------------

   procedure Delete_Tag (Session  : Concrete_Session;
                         Tag_Name : String)
   is
   begin
      if not Tag_Name_Is_Available
               (Session, Tag_Name)
      then
         Session.Internal_Delete_Tag (Tag_Name);
      end if;
   end Delete_Tag;

   -------------------------
   -- Internal_Delete_Tag --
   -------------------------

   procedure Internal_Delete_Tag (Session  : Concrete_Session;
                                  Tag_Name : String)
   is
      Tag_Id : constant Integer := Internal_Tag_Id (Session, Tag_Name);
      AL : Association_List := All_Associations.Filter
                                 (Tag_Field => Tag_Id)
                               .Get (Session_Type (Session));
   begin
      while AL.Has_Row loop
         AL.Element.Detach.Internal_Delete;
         AL.Next;
      end loop;
      Get_Tag (Session_Type (Session), Tag_Id)
      .Internal_Delete;
   end Internal_Delete_Tag;

   ----------------------------
   -- Internal_Put_Resources --
   ----------------------------

   procedure Internal_Put_Resources (Session  : Concrete_Session;
                                     Tag_Name : String)
   is
      AL : Association_List := All_Tags.Filter (Name => Tag_Name)
                               .Get (Session_Type (Session))
                               .Element.Associated_Resources
                               .Get (Session_Type (Session));
   begin
      while AL.Has_Row loop
         Put_Line (Get_Resource (Session_Type (Session),
                                 AL.Element.Resource_Field)
                   .Uri);
         AL.Next;
      end loop;
   end Internal_Put_Resources;

   ---------------------
   -- Internal_Tag_Id --
   ---------------------

   function Internal_Tag_Id (Session  : Concrete_Session;
                             Tag_Name : String)
                            return Integer
   is
      TL : constant Tag_List := All_Tags.Filter (Name => Tag_Name)
                                .Get (Session_Type (Session));
      Internal_Tag_Id_Error : exception;
   begin
      if TL.Has_Row then
         return TL.Element.Id;
      else
         raise Internal_Tag_Id_Error;
      end if;
   end Internal_Tag_Id;

   ------------------
   -- Put_All_Tags --
   ------------------

   procedure Put_All_Tags (Session           : Concrete_Session;
                           With_Descriptions : Boolean := False)
   is
      TL : Tag_List := All_Tags.Get (Session_Type (Session));
   begin
      if With_Descriptions then
         while TL.Has_Row loop
            Put_Line (TL.Element.Name & ": " & TL.Element.Description);
            TL.Next;
         end loop;
      else
         while TL.Has_Row loop
            Put_Line (TL.Element.Name &
                      " (" &
                      Integer'Image (TL.Element.Id) &
                      " )");
            TL.Next;
         end loop;
      end if;
   end Put_All_Tags;

   -------------------
   -- Put_Resources --
   -------------------

   procedure Put_Resources (Session  : Concrete_Session;
                            Tag_Name : String)
   is
      TL : constant Tag_List := All_Tags.Filter (Name => Tag_Name)
                                .Get (Session_Type (Session));
   begin
      if TL.Has_Row then
         Session.Internal_Put_Resources (Tag_Name);
      end if;
   end Put_Resources;

   ---------------------------
   -- Tag_Name_Is_Available --
   ---------------------------

   function Tag_Name_Is_Available (Session  : Concrete_Session;
                                   Tag_Name : String)
                                  return Boolean
   is
      --  use type Tag_List;
      --  This is only here to reproduce a bug with Empty_Tag_List
      TL : constant Tag_List := All_Tags.Filter (Name => Tag_Name)
                                .Get (Session_Type (Session));
   begin
      return not TL.Has_Row;
   end Tag_Name_Is_Available;

end Concrete_Sessions;
