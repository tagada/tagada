--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Ada.Command_Line;             use Ada.Command_Line;
with Tagexp_Model;
with Tagexp_Parser;
with Tag_Expressions.DFS_Evaluate; use Tag_Expressions.DFS_Evaluate;

procedure Search (Session : access Concrete_Session)
is
   Parse_Tree : Tagexp_Model.Parseable_Ptr;
   Visitor : constant DFS_Access := new DFS (Session);
begin
   if Argument_Count = 1 then
      Tagexp_Parser.Run (Argument (1));
      Parse_Tree := Tagexp_Parser.Get_Parse_Tree;
      Parse_Tree.Acceptor (Visitor);
      Visitor.Put_Resources;
   end if;
end Search;
