--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Ada.Command_Line; use Ada.Command_Line;
with Ada.Directories;  use Ada.Directories;

procedure Add (Session : access Concrete_Session)
is
begin
   if Argument_Count > 1 then
      Session.Add_Tag (Argument (1));
      for I in 2 .. Argument_Count loop
         Session.Add_Resource (Full_Name (Argument (I)));
         Session.Add_Association (Argument (1), Full_Name (Argument (I)));
      end loop;
      Session.Commit;
   end if;
end Add;
