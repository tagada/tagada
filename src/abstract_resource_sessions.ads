--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

package Abstract_Resource_Sessions is

   type Abstract_Resource_Session is interface;

   procedure Add_New_Resource (Session      : Abstract_Resource_Session;
                               Resource_Uri : String) is abstract;

   procedure Add_Resource (Session      : Abstract_Resource_Session;
                           Resource_Uri : String) is abstract;

   procedure Add_Resource_Description
               (Session              : Abstract_Resource_Session;
                Resource_Uri         : String;
                Resource_Description : String) is abstract;

   procedure Change_Resource_Uri (Session          : Abstract_Resource_Session;
                                  Resource_Uri     : String;
                                  New_Resource_Uri : String) is abstract;

   procedure Delete_Resource (Session      : Abstract_Resource_Session;
                              Resource_Uri : String) is abstract;

   procedure Internal_Delete_Resource
     (Session      : Abstract_Resource_Session;
      Resource_Uri : String) is abstract;

   procedure Internal_Put_Tags (Session      : Abstract_Resource_Session;
                                Resource_Uri : String) is abstract;

   function Internal_Resource_Id (Session      : Abstract_Resource_Session;
                                  Resource_Uri : String)
                                 return Integer is abstract;

   procedure Put_All_Resources
     (Session : Abstract_Resource_Session) is abstract;

   procedure Put_Tags (Session      : Abstract_Resource_Session;
                       Resource_Uri : String) is abstract;

   function Resource_Uri_Is_Available
     (Session      : Abstract_Resource_Session;
      Resource_Uri : String)
     return Boolean is abstract;

end Abstract_Resource_Sessions;
