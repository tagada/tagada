--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Ada.Directories;
with Ada.Environment_Variables;
with Ada.Unchecked_Deallocation;
with Database;                     use Database;
with GNATCOLL.SQL;                 use GNATCOLL.SQL;
with GNATCOLL.SQL.Exec;            use GNATCOLL.SQL.Exec;
with GNATCOLL.SQL.Sqlite;          use GNATCOLL.SQL.Sqlite;
with GNATCOLL.SQL.Inspect;         use GNATCOLL.SQL.Inspect;

package body Concrete_Sessions.Controlled_Sessions is

   ----------------
   -- Initialize --
   ----------------

   overriding procedure Initialize (Object : in out Controlled_Session)
   is
      DB_File : constant String := Ada.Environment_Variables.Value ("HOME") &
                                   "/" &
                                   ".tagada.db";
      DB_Description : constant Database_Description := Setup (DB_File);
   begin
      if not Ada.Directories.Exists (DB_File) then
         Create_Database (Build_Connection (DB_Description));
      end if;
      Setup (Descr        => DB_Description,
             Max_Sessions => 3);
      Object.Session_Access := new Concrete_Session'(Get_New_Session);
   end Initialize;

   --------------
   -- Finalize --
   --------------

   overriding procedure Finalize (Object : in out Controlled_Session)
   is
      procedure Free is new Ada.Unchecked_Deallocation
                  (Concrete_Session,
                   Concrete_Session_Access);
   begin
      Free (Object.Session_Access);
      GNATCOLL.SQL.Sessions.Free;
   end Finalize;

end Concrete_Sessions.Controlled_Sessions;
