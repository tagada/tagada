--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Ada.Strings.Fixed; use Ada.Strings.Fixed;

package body Tag_Expressions.String_Literals is

   -------------------
   -- Count_Pattern --
   -------------------

   function Count_Pattern (Source : String;
                           Pattern : String)
                          return Natural
   is
      Idx : constant Natural := Index (Source  => Source,
                                       Pattern => Pattern);
   begin
      if Idx = 0 then
         return 0;
      else
         return 1 + Count_Pattern
                      (Source (Idx + Pattern'Length .. Source'Last),
                       Pattern);
      end if;
   end Count_Pattern;

   ---------------------
   -- Remove_Brackets --
   ---------------------

   function Remove_Brackets (Source : String)
                            return String
   is
   begin
      return Head (Source => Tail (Source => Source,
                                   Count  => Source'Length - 1),
                   Count  => Source'Length - 2);
   end Remove_Brackets;

   ---------------------
   -- Replace_Pattern --
   ---------------------

   function Replace_Pattern (Source      : String;
                             Pattern     : String;
                             Replacement : String)
                            return String
   is
      Count : constant Natural := Count_Pattern (Source, Pattern);
      Difference : constant Integer := Pattern'Length - Replacement'Length;
      Idx : Natural;
      Local_Source : String := Source;
   begin
      for I in 1 .. Count loop
         Idx := Index (Source  => Local_Source,
                       Pattern => Pattern);

         Replace_Slice (Source  => Local_Source,
                        Low     => Idx,
                        High    => Idx + Pattern'Length - 1,
                        By      => Replacement,
                        Justify => Ada.Strings.Left);
      end loop;
      return (if Pattern > Replacement then
                 Head (Source => Local_Source,
                       Count  => Source'Length - Count * Difference)
              else
                 Local_Source);
   end Replace_Pattern;

end Tag_Expressions.String_Literals;
