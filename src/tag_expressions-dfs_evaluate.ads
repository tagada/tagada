--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Set_Stacks;        use Set_Stacks;
with Tagexp_DFS;
with Concrete_Sessions; use Concrete_Sessions;
with www_Model;

package Tag_Expressions.DFS_Evaluate is

   type DFS (Session : access Concrete_Session) is new Tagexp_DFS.DFS with
      record
         Stack : Set_Stack;
      end record;

   type DFS_Access is access all DFS'Class;

   overriding procedure After_www_nonterminal2
     (I : access DFS;
      N : access www_Model.www_nonterminal2'Class);

   overriding procedure After_xxx_nonterminal2
     (I : access DFS;
      N : access xxx_Model.xxx_nonterminal2'Class);

   overriding procedure After_yyy_nonterminal2
     (I : access DFS;
      N : access yyy_Model.yyy_nonterminal2'Class);

   overriding procedure Visit_zzz_nonterminal1
     (I : access DFS;
      N : access zzz_Model.zzz_nonterminal1'Class);

   not overriding procedure Put_Resources (I : access DFS);

end Tag_Expressions.DFS_Evaluate;
