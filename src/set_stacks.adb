--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

package body Set_Stacks is

   ----------
   -- Push --
   ----------

   procedure Push (S   : in out Set_Stack;
                   Set : Resource_Uri_Set)
   is
   begin
      S.Append (Set);
   end Push;

   ---------
   -- Pop --
   ---------

   function Pop (S : in out Set_Stack)
                return Resource_Uri_Set
   is
      Top_Set : Resource_Uri_Set;
   begin
      if S.Is_Empty then
         raise Set_Stack_Empty;
      else
         Top_Set := S.Last_Element;
         S.Delete_Last;
      end if;
      return Top_Set;
   end Pop;

   ----------
   -- Size --
   ----------

   function Size (S : Set_Stack) return Natural
   is
   begin
      return Natural (S.Length);
   end Size;

end Set_Stacks;
