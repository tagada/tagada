--------------------------------------------------------------------------
--                                                                      --
--                             T a g A d a                              --
--                                                                      --
--                Copyright (C) 2011-2013, Samy Mahmoudi                --
--                                                                      --
-- This program is free software: you can redistribute it and/or modify --
-- it under the terms of the GNU General Public License as published by --
-- the Free Software Foundation, either version 3 of the License, or    --
-- (at your option) any later version.                                  --
--                                                                      --
-- This program is distributed in the hope that it will be useful,      --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of       --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the         --
-- GNU General Public License for more details.                         --
--                                                                      --
-- You should have received a copy of the GNU General Public License    --
-- along with this program. If not, see <http://www.gnu.org/licenses/>. --
--                                                                      --
--------------------------------------------------------------------------

with Resource_Uri_Sets;               use Resource_Uri_Sets;
with Tag_Expressions.String_Literals; use Tag_Expressions.String_Literals;

package body Tag_Expressions.DFS_Evaluate is

   ----------------------------
   -- After_www_nonterminal2 --
   ----------------------------

   procedure After_www_nonterminal2
     (I : access DFS;
      N : access www_Model.www_nonterminal2'Class)
   is
   begin
      I.Stack.Push (Union (I.Stack.Pop, I.Stack.Pop));
   end After_www_nonterminal2;

   ----------------------------
   -- After_xxx_nonterminal2 --
   ----------------------------

   procedure After_xxx_nonterminal2
     (I : access DFS;
      N : access xxx_Model.xxx_nonterminal2'Class)
   is
   begin
      I.Stack.Push (Intersection (I.Stack.Pop, I.Stack.Pop));
   end After_xxx_nonterminal2;

   ----------------------------
   -- After_yyy_nonterminal2 --
   ----------------------------

   procedure After_yyy_nonterminal2
     (I : access DFS;
      N : access yyy_Model.yyy_nonterminal2'Class)
   is
      Right : constant Resource_Uri_Set := I.Stack.Pop;
      Left  : constant Resource_Uri_Set := I.Stack.Pop;
   begin
      I.Stack.Push (Left.Difference (Right));
   end After_yyy_nonterminal2;

   ----------------------------
   -- Visit_zzz_nonterminal1 --
   ----------------------------

   procedure Visit_zzz_nonterminal1
     (I : access DFS;
      N : access zzz_Model.zzz_nonterminal1'Class)
   is
   begin
      I.Stack.Push
        (Get_Resource_Uri_Set
           (I.Session.all,
            Replace_Pattern (Remove_Brackets
                               (N.String_Literal_Part.Token_String.all),
                             Pattern => """""",
                             Replacement => """")));
   end Visit_zzz_nonterminal1;

   -------------------
   -- Put_Resources --
   -------------------

   procedure Put_Resources (I : access DFS)
   is
      Evaluation_Error : exception;
   begin
      if I.Stack.Size = 1 then
         I.Stack.Pop.Put_Resources (I.Session.all);
      else
         raise Evaluation_Error;
      end if;
   end Put_Resources;

end Tag_Expressions.DFS_Evaluate;
