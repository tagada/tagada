#!/bin/sh

cd src/automatically_generated
   # Generate the database-related packages
   gnatcoll_db2ada -api=Database -orm=ORM -dbmodel=../db.schema -adacreate
   # Patch the source file database.ads to make Database_Connection visible
   patch < ../diffs/database.ads.diff
   # Change internal package name Orm into ORM (Style)
   sed -e 's/Orm/ORM/g' -i ".orig" orm.ads orm.adb
cd ../..
